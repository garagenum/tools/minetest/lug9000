
local storage = minetest.get_mod_storage()
local __player_data
local __mcq_data

-- Table Save Load Functions
function mcq_editor.save()
	print("save called")
	storage:set_string("mcq_data",minetest.serialize(__mcq_data))
	storage:set_string("player_data",minetest.serialize(__player_data))
end

function mcq_editor.load()
	print("load called")
	__player_data = minetest.deserialize(storage:get_string("player_data")) or {}
	__mcq_data = minetest.deserialize(storage:get_string("mcq_data")) or {}
end

function mcq_editor.mcq(question)
	assert(type(question) == "string")
	local mcqdata = __mcq_data[question] or {}
	mcqdata.prizes = mcqdata.prizes or {}
	__mcq_data[question] = mcqdata
	return mcqdata
end
	

function mcq_editor.player(name)
	assert(type(name) == "string")
	local data = __player_data[name] or {}
	data.unlocked = data.unlocked or {}
	data.locks = data.locks or {}
	data.attempts = data.attempts or {}	
	data.name     = data.name or name
	__player_data[name] = data
	return data
end


function mcq_editor.player_or_nil(name)
	return __player_data[name]
end

function mcq_editor.enable(name)
	mcq_editor.player(name).disabled = nil
end

function mcq_editor.disable(name)
	mcq_editor.player(name).disabled = true
end

function mcq_editor.clear_player(name)
	__player_data[name] = {}
end
