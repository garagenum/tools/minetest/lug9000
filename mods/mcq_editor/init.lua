mcq_editor = {}
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)
print("we are in : "..modpath)

minetest.register_privilege("teach", {
	description = "Player can change qcm nodes and edit questions",
	give_to_singleplayer= false,
})

-- Use save/load functions from mod gnapi
print("we will call sav")
dofile(modpath.."/gnapi/save.lua")
mcq_editor.load()

minetest.register_on_shutdown(function()
	level_editor.save()
end)

dofile(modpath.."/gnapi/utils.lua")
dofile(modpath.."/gnapi/selector.lua")

dofile(modpath.."/src/questions.lua")
dofile(modpath.."/src/answers.lua")
mcq_editor.register_questions()

dofile(modpath.."/src/nodes.lua")
mcq_editor.custom_mcq_nodes()
