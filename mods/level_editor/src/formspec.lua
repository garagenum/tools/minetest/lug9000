local function register_messages_pics()
	local fheight = 11
	local fwidth = 15
	level_editor.message_formspec = "size["..fwidth..","..fheight..",false]"..
		"no_prepend[]"..
		"label[2,0.2;Click on picture infos]"
	local level_dir = minetest.get_modpath("level_editor").."/textures/"
	local base_levels = minetest.get_dir_list(level_dir,false)
	local p,c = math.modf(math.sqrt(fheight))
	local messages = {}
	for i,v in ipairs(base_levels) do
		if level_editor.check_file(v) and v:match("^info") then
			table.insert(messages,v)
		end
	end
	
	for i,v in pairs(messages) do
		local e,r = math.modf((i-1)/p)
		local xpos = e*p
	--	if i <= fheight-7 then
		local ypos = i-xpos
		ypos = ypos*2+(ypos-1) 				
		local length = #messages
		local f,s = math.modf((length-1)/p)
		xpos = xpos+(fwidth/2-(2*f-1)) 
		level_editor.message_formspec = level_editor.message_formspec.."image_button["..xpos..","..ypos..";2,2;message_"..v..".png;"..v..";]"
	end
end
register_messages_pics()
