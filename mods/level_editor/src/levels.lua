local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

local on_receive_level_access_fields = nil
on_receive_level_access_fields = function(pos, formname, fields, placer)
	print("fields received for level access: "..dump(fields))
	local name = placer:get_player_name()
	if not minetest.check_player_privs(name, {level=true}) then
		minetest.chat_send_player(name,"Tu n'as pas le droit de modifier le formulaire")
		return
	else
		for i,v in pairs(fields) do
			if i ~= "quit" and i ~= "clear" then
				minetest.chat_send_player(name,"Level : "..i)
				local node = minetest.get_node(pos)
				node.name = "level_editor:level_"..i
				print("new name : "..node.name)
				minetest.swap_node(pos,node)
			elseif i == "clear" then
				local node = minetest.get_node(pos)
				node.name = "level_editor:level"
				print("new name : "..node.name)
				minetest.swap_node(pos,node)
				minetest.chat_send_player(name,"No info for this level access")
			end
		end
	end
end

local on_punch_level_access = nil
on_punch_level_access = function(pos,node,player,pointed_thing)
	local name = player:get_player_name()
	local _,level = string.match(node.name, "^("..modname..":level_)(.+)")
	local keys=player:get_player_control()
	if minetest.check_player_privs(name, {level=true}) and keys["aux1"]==true then
		return -- if sneaking, on_punch calls normal destruct function
	elseif level then
		level_editor.go_level(name,level)
	else
		local formdef = level_editor.make_formspec("textures","level_")
		formdef = formdef..
			"label[2,0;Click on level to modify]"
		local formname = "level_editor.level_chooser"
		minetest.show_formspec(name,formname,formdef)
	end
end

level_editor.register_levels_from_files = function()
	local level_dir = modpath.."/levels/"
	local base_levels = minetest.get_dir_list(level_dir,false)  -- false returns only file names
	local textures_dir = modpath.."/textures/"
	local base = "level"

	for i,v in ipairs(base_levels) do
		if v:match("(%.conf)$") then
			local level,_ = v:match("(.-)(%.conf)$")
			local leveldata = level_editor.getleveldata(level)

			local settings = Settings(level_dir..v)
			local conf = settings:to_table()

			local picture = level_editor.check_file(textures_dir..base.."_"..conf.name..".png")
			if picture then
				conf.picture = base.."_"..conf.name..".png"
			else
				conf.picture = "level.png"
			end

			if conf.infos then
				local infos = {}
				for info in conf.infos:gmatch("[^,]+") do
					table.insert(infos,info)
				end
				conf.infos = infos
			else conf.infos = {}
			end

			if conf.privs then
				local privs = {}
				for priv in conf.privs:gmatch("[^,]+") do
				  table.insert(privs,priv)
				end
				conf.privs = privs
			end

			conf.basepoint = minetest.string_to_pos(conf.basepoint)
			conf.startpoint = minetest.string_to_pos(conf.startpoint)

			conf.size = minetest.string_to_pos(conf.size) or level_editor.default_size

			if not conf.endpoint then
				conf.endpoint = {}
				for k,v in pairs(conf.size) do
					conf.endpoint[k] = conf.basepoint[k] + v
				end
			else conf.endpoint = minetest.string_to_pos(conf.endpoint)
			end

			if conf.protection_start then
				conf.protection_start = minetest.string_to_pos(conf.protection_start)
			end
			if conf.protection_end then
				conf.protection_end = minetest.string_to_pos(conf.protection_end)
			end
			if conf.protection and conf.protection == "all" then
				conf.protection_start = table.copy(conf.basepoint)
				conf.protection_end = table.copy(conf.endpoint)
	--		else conf.protection = ""
			end
	--		if not conf.protection_start then conf.protection_start = {} end
	--		if not conf.protection_end then conf.protection_end = {} end

	--		if not conf.owned then conf.owned = "false" end
	--		if not conf.jump then conf.jump = 1 end
	--		if not conf.hp then conf.hp = 16 end
	--		if not conf.speed then conf.speed = 1 end
	--		if not conf.gravity then conf.gravity = 1 end

			print("name "..type(conf["name"]).." "..conf.name)
			leveldata.base_def = table.copy(conf)
			level_editor.save()
			table.insert(level_editor.registered_levels,conf.name)
		end
	end
	local folder = "textures"

	level_editor.register_images_as_nodes(folder,base,base.."_",base)
end
level_editor.register_levels_from_files()

local custom_level_nodes = nil
custom_level_nodes = function()
	for nodename,nodedef in pairs(minetest.registered_nodes) do
		if nodename:match("^(level_editor:level)") then
			print(nodename.." matching. custom it")
			minetest.override_item(nodename,{on_punch = on_punch_level_access})
			minetest.override_item(nodename,{on_dig = level_editor.can_dig})
			minetest.override_item(nodename,{on_receive_fields = on_receive_level_access_fields})
			if nodename ~= "level_editor:level" then
	--			minetest.override_item(nodename,{tiles = {"checkpoint.png"}})
			end
		end
	end
end
custom_level_nodes()


level_editor.register_level = function(name,level)
	local player = minetest.get_player_by_name(name)
	local pldata = level_editor.getplayerdata(name)
	local leveldata = level_editor.getleveldata(level)

	local count_gen = leveldata.count_gen +1

	local ldef = {}
	ldef = table.copy(leveldata.base_def)

	local _,size = level_editor.get_schem(level)
	ldef.size = size or ldef.size

	for k,v in pairs(ldef.size) do
		ldef.endpoint[k] = ldef.basepoint[k] + v
	end

	if ldef.mode and ldef.mode == "single" then
			ldef.basepoint.x = ldef.basepoint.x + ldef.size.x * (count_gen - 1)
		--	ldef.basepoint.y = ldef.basepoint.y + ldef.size.y * (count_gen - 1)
		--	ldef.basepoint.z = ldef.basepoint.z + ldef.size.z * (count_gen - 1)
			ldef.endpoint.x = ldef.endpoint.x + ldef.size.x * (count_gen - 1)
		if ldef.protection_start then
			ldef.protection_start.x = ldef.protection_start.x + ldef.size.x * (count_gen - 1)
		end
		if ldef.protection_end then
			ldef.protection_end.x = ldef.protection_end.x + ldef.size.x * (count_gen - 1)
		end
	end

	ldef.realstartpoint = {}
	for i,v in pairs(ldef.basepoint) do
		ldef.realstartpoint[i] = ldef.basepoint[i] + ldef.startpoint[i]
	end

	if ldef.protection and ldef.protection == "all" then
		ldef.protection_start = table.copy(ldef.basepoint)
		ldef.protection_end = table.copy(ldef.endpoint)
	end

	pldata.registered_levels[level] = table.copy(ldef)
	leveldata.count_gen = count_gen
	--leveldata.base_def = table.copy(ldef)
	level_editor.save()

	--ldef = {}
	--ldef = pldata.registered_levels[level]

	if	(ldef.mode and ldef.mode == "single") or leveldata.count_gen == 1 then
		level_editor.put_schem(name,level)
	--	print("protection : "..ldef.protection.." start "..dump(ldef.protection_start).." end : "..dump(ldef.protection_end))
		if ldef.protection or (ldef.protection_start  and ldef.protection_end ) then
			local area_name = level.."_"..ldef.protection_start.x
			if ldef.owned and ldef.owned =="true" then
				print("protected by "..name)
				level_editor.protect_area(ldef.protection_start ,ldef.protection_end,area_name,name)
			else level_editor.protect_area(ldef.protection_start ,ldef.protection_end,area_name)
				print("protected by no one")
			end
		end
	end

--[[
	if not (ldef.mode and ldef.mode == "single") and leveldata.count_gen > 1 then
	else
		level_editor.put_schem(name,level)
	end
--]]
	if ldef.registercall then
		ldef.registercall()
	end
	minetest.log("action","player "..name.." registered new level :"..level)
end

level_editor.go_level = function(name, level)
	local player = minetest.get_player_by_name(name)

		--Terminate previous level
	local pldata = level_editor.getplayerdata(name)
	local oldlev = pldata.actual_level
	print("oldlev : "..oldlev)
	if oldlev ~= "" then
		local oldldef = pldata.registered_levels[oldlev]
		if oldldef.endcall then
			oldldef.endcall(player)
		end

		if oldldef.hp then
			player:set_hp(20)
		end
	end

		-- Save datas
	minetest.log("action", name.." has entered level "..level)
	pldata.actual_level = level
	level_editor.save()

		-- Register level if not yet
	local ldef = pldata.registered_levels[level]
	if not ldef then
		level_editor.register_level(name,level)
		pldata = level_editor.getplayerdata(name)
	end
		-- Enter in new level
	local nldef = pldata.registered_levels[level]
	if nldef.realstartpoint then
	--[[
		local realstartpoint = {}
		for k,v in pairs(nldef.realstartpoint) do
			realstartpoint[k] = v
		end
	--]]
		local node = minetest.get_node(nldef.realstartpoint)
		local strpos = minetest.pos_to_string(nldef.realstartpoint)
		local chdata = level_editor.getcheckpointdata(strpos)
		chdata.infos = nldef.infos or {}
		level_editor.activate_checkpoint(name,nldef.realstartpoint,node)
		level_editor.go_checkpoint(name)
		pldata = level_editor.getplayerdata(name)
	end
	if level == "deplacements" then
	--	minetest.settings:set_bool("enable_pvp",true)
	--minetest.settings:set_bool("enable_damage",false)
		minetest.settings:write()
	end
	if nldef.startcall then
		nldef.startcall()
	end

	if nldef.hp then
		player:set_hp(nldef.hp)
	end

	local privs = {}
	if nldef.privs then
		for i,v in ipairs(nldef.privs) do
			privs[v] = true
		end
	end
	privs.interact = true
	privs.shout = true
	minetest.set_player_privs(name,privs)


	local physics = player:get_physics_override()
	if nldef.speed then
		physics.speed = nldef.speed
	else physics.speed = 1
	end
	if nldef.gravity then
		physics.gravity = nldef.gravity
	else physics.gravity = 1
	end
	if nldef.jump then
		physics.jump = nldef.jump
	else physics.jump = 1
	end
	player:set_physics_override(physics)
end

minetest.register_on_player_receive_fields(function(player,formname,fields)
	if not formname:match("^(level_editor.goto_chooser)") then
		return false
	end
	print("fields for "..formname.." :: "..dump(fields))
	local _,dest = formname:match("^(level_editor.goto_chooser_)(.+)")
	for k,v in pairs(fields) do
		if k ~= "quit" and k ~= "ok" then
			if dest == "all" then
				for _,player in ipairs(minetest.get_connected_players()) do
					local name = player:get_player_name()
					level_editor.go_level(name,k)
				end
			else
				level_editor.go_level(dest,k)
			end
			return
		end
	end
end)


minetest.register_chatcommand("goto", {
	params = "< enfant | * for allplayers>,",
	privs = "level",
	description = "Envoyer un joueur (ou tous les joueurs *) directement à un niveau",
	func = function(sender,options)
		local formdef = level_editor.make_formspec("textures","level_")
		formdef = formdef..
			"label[2,0;Click on a level and then OK !]"..
			"button_exit[13,0;2,1;ok;OK]"
		local formname = "level_editor.goto_chooser"
		local dest = string.match(options,"([%S]+)")
		if not dest then
			formname = formname.."_"..sender
			minetest.show_formspec(sender,formname,formdef)
		elseif dest == "*" then
			formname = formname.."_all"
			minetest.show_formspec(sender,formname,formdef)
		else
			formname = formname.."_"..dest
			minetest.show_formspec(sender,formname,formdef)
		end
	end
})
