local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)
local tcol = "#008000"
local scol = "#FFA500"
local fs_b1 = 1

local allprivs = {}
local Sallprivs = ""
for k,v in pairs(minetest.registered_privileges) do
	table.insert(allprivs,k)
	Sallprivs = Sallprivs..k..","
end
Sallprivs,_ = Sallprivs:match("(.+)(,)$")

local mk_fs = nil
mk_fs = function(name,level,...)
	local leveldata = level_editor.getleveldata(level)
	local ldef = {}
	ldef = table.copy(leveldata.base_def)
	print("dump ldef at mk_fs in editor : "..dump(ldef))

	ldef.name = ldef.name or level
	ldef.picture = ldef.picture or "level.png"

	ldef.basepoint = minetest.pos_to_string(ldef.basepoint):match("[^()]+")
	ldef.realstartpoint = minetest.pos_to_string(ldef.realstartpoint):match("[^()]+")
	ldef.startpoint = minetest.pos_to_string(ldef.startpoint):match("[^()]+")
	ldef.endpoint = minetest.pos_to_string(ldef.endpoint):match("[^()]+")

	ldef.hp = ldef.hp or 16
	ldef.jump = ldef.jump or 1
	ldef.gravity = ldef.gravity or 1
	ldef.speed = ldef.speed or 1

	if ldef.protection and ldef.protection == "all" then
		ldef.protection_start = ldef.basepoint
		ldef.protection_end = ldef.endpoint
	elseif ldef.protection_start and ldef.protection_end then
		ldef.protection = ""
		ldef.protection_start = minetest.pos_to_string(ldef.protection_start):match("[^()]+")
		ldef.protection_end = minetest.pos_to_string(ldef.protection_end):match("[^()]+")
	else
		ldef.protection_start = "no"
		ldef.protection_end = "no"
	end

	local is_single_true = "false"
	if ldef.mode and ldef.mode == "single" then
		is_single_true = "true"
	end
	local is_owned_true = "false"
	if ldef.protection and ldef.owned and ldef.owned == "true" then
		is_owned_true = "true"
	end

	local actualprivs = "shout,interact,"
	if ldef.privs then
		for i,v in ipairs(ldef.privs) do
			actualprivs = actualprivs..v..","
		end
	end
	actualprivs = actualprivs:match("(.+)(,)$")

	local level_dir = modpath.."/levels/"
	local settings = Settings(level_dir..ldef.name..".conf"):to_table()
	local save = "Save"
	save = minetest.colorize(tcol,save)

	local schem = "Schem"
	schem = minetest.colorize(scol,schem)


--	print("base_def at gui show : "..dump(ldef))
	local formspec = "size[15,8]" ..
				"container[0," .. math.random() * 1000 .. "]container_end[]".. --container forces update
				"label[0.5,0;Basic definition]"..
				"checkbox[3,-0.2;single;Single mode;"..is_single_true.."]"..
				"button[0,1;3,1;changename;Re-Name]"..
				"field[3.2,1.3;3,1;name;;"..ldef.name.."]" ..
				"field_close_on_enter[name;false]"..
				"button[0,2;3,1;startpoint;New start]"..
				"label[3,2.2;"..ldef.realstartpoint.."]"..
				"button[0,3;3,1;basepoint;New base]"..
				"label[3,3.2;"..ldef.basepoint.."]"..
				"button[0,4;3,1;endpoint;New end]"..
				"label[3,4.2;"..ldef.endpoint.."]"..
				"button[6.8,0.5;2,1;schem;"..schem.."]"..
				"button[9,0.5;2,1;save;"..save.."]"..
				"image[11,0;2,2;"..ldef.picture.."]"..
				"label[7,1.5;Protect :]"..
				"button[6.8,2;1,1;protectall;All]"..
				"button[8,2;1,1;protectnone;None]"..
				"checkbox[9.5,2;owned;Owned;"..is_owned_true.."]"..
				"button[6.5,3;3,1;protectionstart;Protection start]"..
				"label[9.5,3.2;"..ldef.protection_start.."]"..
				"button[6.5,4;3,1;protectionend;Protection end]"..
				"label[9.5,4.2;"..ldef.protection_end.."]"..
				"button[12.5,2;2,1;infos;Infos]"..
				"label[1,5.2;Player Attributes]"..
				"button[0,6;1.4,1;changehp;HP(1-16)]"..
				"field[1.6,6.3;1,1;hp;;"..ldef.hp.."]"..
				"button[0,7;1.4,1;changejump;Jump]"..
				"field[1.6,7.3;1,1;jump;;"..ldef.jump.."]"..
				"button[2.5,6;1.4,1;changegrav;Gravity]"..
				"field[4.1,6.3;1,1;grav;;"..ldef.gravity.."]"..
				"button[2.5,7;1.4,1;changespeed;Speed]"..
				"field[4.1,7.3;1,1;speed;;"..ldef.speed.."]"..
				"label[6.5,5.2;Privs: Actual ]"..
				"textlist[6.5,6;3,2;actualprivs;"..actualprivs.."]"..
				"label[9.9,5.2;Available]"..
				"textlist[9.8,6;3,2;allprivs;"..Sallprivs.."]"
	for i,v in ipairs(ldef.infos) do
		formspec = formspec..
			"label[12.5,"..2.5+0.5*i..";"..ldef.infos[i].."]"
	end
	return formspec
end


local save_level_conf = nil
save_level_conf = function(plname,level)
	local level_dir = modpath.."/levels/"
	os.remove(level_dir..level..".conf")
	local settings = Settings(level_dir..level..".conf")
	local leveldata = level_editor.getleveldata(level)
	local base_def = table.copy(leveldata.base_def)
	local levelname = base_def.name or level
	print("name in save is : "..levelname)
	settings:set("name",levelname)
	settings:set("basepoint",minetest.pos_to_string(base_def.basepoint):match("[^()]+"))
	settings:set("startpoint",minetest.pos_to_string(base_def.startpoint):match("[^()]+"))
	settings:set("endpoint",minetest.pos_to_string(base_def.endpoint):match("[^()]+"))

	local pos_options = {"protection_start","protection_end"}
	for i,v in ipairs(pos_options) do
		if base_def[v] then
			settings:set(v,minetest.pos_to_string(base_def[v]):match("[^()]+"))
		end
	end

	if base_def.privs then
		local privs_to_string = ""
		for i,v in ipairs(base_def.privs) do
			privs_to_string = privs_to_string..v..","
		end
		privs_to_string,_ = privs_to_string:match("(.+)(,)$")
		base_def.privs = privs_to_string
	end

	if base_def.infos then
		local infos_to_string = ""
		for i,v in ipairs(base_def.infos) do
			infos_to_string = infos_to_string..v..","
		end
		infos_to_string = infos_to_string:match("(.+)(,)$")
		base_def.infos = infos_to_string
	end

	local other_options = {"protection","mode","owned","jump","gravity","hp","speed","privs","infos"}
	for i,v in ipairs(other_options) do
		if base_def[v] then
			settings:set(v,base_def[v])
		end
	end

	local success = settings:write()
	print("on save click, settings get : "..dump(settings:to_table()))
	minetest.log("action",plname.." recorded new definition for level "..levelname)--base_def.name)
end

minetest.register_on_player_receive_fields(function(player,formname,fields)
	local plname = player:get_player_name()
	local fname,oldname,_,newname = formname:match("^(level_editor:confirm_erase_)(.+)(_by_)(.+)")
	if not fname then return false end
	print("fields: "..dump(fields))
	local oldata = level_editor.getleveldata(oldname)
	local oldef = oldata.base_def
	local ndata = level_editor.getleveldata(newname)
	local ndef = ndata.base_def

	for k,v in pairs(fields) do
		if k == "erase" then
			tcol = "#FF0000"
			print("erased")
			ndef = table.copy(oldef)
			ndef.name = newname
			oldef = {}
			oldata.base_def = table.copy(oldef)
			local index = table.indexof(level_editor.registered_levels,oldname)
			table.remove(level_editor.registered_levels,index)
			ndata.base_def = table.copy(ndef)
			level_editor.save()
			table.insert(level_editor.registered_levels,ndef.name)
			os.remove(modpath.."/levels/"..oldname..".conf")
			os.rename(modpath.."/levels/"..oldname..".mts",modpath.."/levels/"..ndata.base_def.name..".mts")
			save_level_conf(plname,ndef.name)
			level_editor.gui(plname,ndef.name)
			print("dump levels registered : "..dump(level_editor.registered_levels))
		elseif k == "cancel" then
			level_editor.gui(plname,oldef.name)
		end
	end
end)

local changename = nil
changename = function(plname,level,fields)
	local leveldata = level_editor.getleveldata(level)
	local base_def = table.copy(leveldata.base_def)
	base_def.name = base_def.name or level
	print("level : "..level)
	if level == "home" then
		minetest.show_formspec(plname,"level_editor:home_cant_be_changed",
		"size[5,2]"..
		"label[0,0;Le niveau "..level.." \nne peut pas changer de nom]")
		return
	end
	for i,v in ipairs(level_editor.registered_levels) do
		print("a level registered is "..v.." while name = "..fields.name)
		if fields.name == v then
			print("so "..v.." was matching")
			local formname = "level_editor:confirm_erase_"..base_def.name.."_by_"..v
			minetest.show_formspec(plname, formname,
			"size[5,2]" ..
			"label[0,0;Le niveau "..v.."existe déjà, \nvoulez-vous l'écraser?]" ..
			"button_exit[0,1;2,1;erase;ERASE]"..
			"button_exit[3,1;2,1;cancel;CANCEL]")
			return
		end
	end
	tcol = "#FF0000"
	scol = "#FFA500"
	base_def.name = fields.name
	local ndata = level_editor.getleveldata(base_def.name)
	ndata.base_def = table.copy(base_def)
	print("but after copy we just have : "..dump(ndata.base_def))
	local index = table.indexof(level_editor.registered_levels,level)
	print("indexed : "..level_editor.registered_levels[index])
	table.remove(level_editor.registered_levels,index)
	base_def = {}
	leveldata.base_def = table.copy(base_def)
	level_editor.save()
	os.remove(modpath.."/levels/"..level..".conf")
	save_level_conf(plname,ndata.base_def.name)
	table.insert(level_editor.registered_levels,ndata.base_def.name)
	os.rename(modpath.."/levels/"..level..".mts",modpath.."/levels/"..ndata.base_def.name..".mts")
	print("dump levels registered (no erase): "..dump(level_editor.registered_levels))
	level_editor.gui(plname,ndata.base_def.name)
end

minetest.register_on_player_receive_fields(function(player,formname, fields)
	local verif,level = formname:match("^(level_editor:infos_selector_)(.+)")
	if not (verif and level) then
		return false
	end
	print("fields received : "..dump(fields))
	local name = player:get_player_name()
	if not minetest.check_player_privs(name, {level=true}) then
		minetest.chat_send_player(name,"Tu n'as pas le droit de modifier le formulaire")
		return
	else
		local pldata = level_editor.getplayerdata(name)
		local leveldata = level_editor.getleveldata(level)
		local base_def = leveldata.base_def
		for k,v in pairs(fields) do
			if k ~= "quit" and k ~= "clear" and k~= "ok" then
				table.insert(base_def.infos,k)
				leveldata.base_def = base_def
				level_editor.save()
			elseif k == "clear" then
				base_def.infos = {}
				leveldata.base_def = base_def
				level_editor.save()
			end
		end
	end
end)


minetest.register_on_player_receive_fields(function(player,formname, fields)
	print("we are in receive fields")
	print("formname is "..formname)
	if not formname:match("^(level_editor:main_gui_)") then
		return false
	end

	print("formname "..formname)
	local _,level = formname:match("(level_editor:main_gui_)(.+)")
	local plname = player:get_player_name()
	print("players name is "..plname)
	if not level then
		minetest.show_formspec(plname,"level_editor:no-name-for-level",
			"size[3,2]"..
			"label[0,0;Vous devez donner \n un nom au niveau]"..
			"button_exit[1,2;1,1;exit;Ok]")
		return
	end

	local leveldata = level_editor.getleveldata(level)
	local base_def = table.copy(leveldata.base_def)
	print("level called is "..level)
	base_def.name = base_def.name or level
	print("so name is "..base_def.name)

	if fields.changename then
		changename(name,level,fields)
		return
	end

	if fields.basepoint then
		tcol = "#FF0000"
		scol = "#FFA500"
		local oldbase = base_def.basepoint
		local oldend = base_def.endpoint
		local oldreal = base_def.realstartpoint
		base_def.basepoint = level_editor.round(player:get_pos())
		for k,v in pairs(base_def.basepoint) do
			base_def.realstartpoint[k] = oldreal[k] - oldbase[k] + v
			base_def.endpoint[k] = oldend[k] - oldbase[k] + v
		end
	end

	if fields.startpoint then
		tcol = "#FF0000"
		base_def.realstartpoint = level_editor.round(player:get_pos())
--		print("real: "..dump(base_def.realstartpoint))
--		print("base: "..dump(base_def.basepoint))
--		print("start: "..dump(base_def.startpoint))
		if not base_def.basepoint then
			base_def.startpoint = {x=0,y=0,z=0}
		else
			for k,v in pairs(base_def.realstartpoint) do
				base_def.startpoint[k] = v - base_def.basepoint[k]
			end
			print("relative start point : "..minetest.pos_to_string(base_def.startpoint))
		end
	end

	if fields.endpoint then
		tcol = "#FF0000"
		scol = "#FFA500"
		base_def.endpoint = level_editor.round(player:get_pos())
	end

	if fields.schem then
		scol = "#008000"
		local filename = modpath.."/levels/"..base_def.name..".mts"
		local schem = minetest.create_schematic(base_def.basepoint,base_def.endpoint,nil,filename,nil)
		local basepoint = minetest.pos_to_string(base_def.basepoint)
		local endpoint = minetest.pos_to_string(base_def.endpoint)
		if schem then
			print(base_def.name..".mts created from "..basepoint.." to "..endpoint)
			minetest.chat_send_player(plname,base_def.name..".mts created from "..basepoint.." to "..endpoint)
		end
	end

	if fields.single then
		tcol = "#FF0000"
		if fields.single == "true" then
			base_def.mode = "single"
		else base_def.mode = nil
		end
	end

	if fields.changehp and tonumber(fields.hp) and tonumber(fields.hp) <= 16 and tonumber(fields.hp) >= 1 then
		tcol = "#FF0000"
		base_def.hp = tonumber(fields.hp)
	end

	if fields.changejump and tonumber(fields.jump) and tonumber(fields.jump) > 0 then
		tcol = "#FF0000"
		base_def.jump = tonumber(fields.jump)
	end

	if fields.changegrav and tonumber(fields.grav) and tonumber(fields.grav) > 0 then
		tcol = "#FF0000"
		base_def.gravity = tonumber(fields.grav)
	end
	if fields.changespeed and tonumber(fields.speed) and tonumber(fields.speed) > 0 then
		tcol = "#FF0000"
		base_def.speed = tonumber(fields.speed)
	end

	if fields.allprivs then
		print("allfields : "..dump(fields.allprivs))
		local event = minetest.explode_textlist_event(fields.allprivs)
		if event.type == "DCL" then
			local priv = allprivs[event.index]
			if priv == "interact" or priv == "shout" then
				return
			end
			tcol = "#FF0000"
			print("priv : "..priv)
			local waspriv = nil
			base_def.privs = base_def.privs or {}
			for i,v in ipairs(base_def.privs) do
				if v == priv then
					waspriv = i
				end
			end
			if waspriv ~= nil then
					table.remove(base_def.privs,waspriv)
			else table.insert(base_def.privs,priv)
			end
			print("dump lprivs "..dump(base_def.privs))
		end
	end

	if fields.protectall then
		tcol = "#FF0000"
		base_def.protection = "all"
	end
	if fields.protectnone then
		tcol = "#FF0000"
		base_def.protection = nil
		base_def.protection_start = nil
		base_def.protection_end = nil
	end
	if fields.owned then
		tcol = "#FF0000"
		if fields.owned == "true" then
			base_def.owned = "true"
		else base_def.owned = "false"
		end
	end
	if fields.protectionstart then
		tcol = "#FF0000"
		base_def.protection = nil
		base_def.protection_start = level_editor.round(player:get_pos())
		base_def.protection_end = base_def.protection_end or table.copy(base_def.endpoint)
	end
	if fields.protectionend then
		tcol = "#FF0000"
		base_def.protection = nil
		base_def.protection_end = level_editor.round(player:get_pos())
		base_def.protection_start = base_def.protection_start or table.copy(base_def.startpoint)
	end
	if fields.save then
		save_level_conf(plname,base_def.name)
		tcol = "#008000"
	end

	if fields.infos then
		local formname = "level_editor:infos_selector_"..base_def.name
		print("formname : "..formname)
		local formdef = level_editor.make_formspec("textures","info_")
		formdef = formdef..
			"label[2,0;Click on picture infos]"..
			"button_exit[13,0;2,1;ok;OK]"..
			"button_exit[13,1;2,1;clear;Clear]"
		minetest.show_formspec(plname,formname,formdef)
		return true
	end

	if fields.quit == "true" then
		return true
	end

	leveldata.base_def = table.copy(base_def)
	level_editor.save()
	level_editor.gui(plname,base_def.name)

	return true
end)

function level_editor.gui(plname,level)
	local leveldata = level_editor.getleveldata(level)
	local ldef = table.copy(leveldata.base_def)
	local player = minetest.get_player_by_name(plname)

	ldef.basepoint = ldef.basepoint or level_editor.round(player:get_pos())
	ldef.realstartpoint = ldef.realstartpoint or level_editor.round(player:get_pos())

	local startpoint = {}
	for k,v in pairs(ldef.basepoint) do
		startpoint[k] = ldef.realstartpoint[k] - v
	end
	ldef.startpoint = ldef.startpoint or startpoint

	ldef.size = ldef.size or level_editor.default_size

	local endpoint = {}
	for k,v in pairs(ldef.basepoint) do
		endpoint[k] = v + ldef.size[k]
	end
	ldef.endpoint = ldef.endpoint or endpoint

	ldef.infos = ldef.infos or {}

	leveldata.base_def = table.copy(ldef)
	level_editor.save()
	table.insert(level_editor.registered_levels,level)

	minetest.chat_send_player(plname,"defining level "..level)--data.base_def.name)
	local formspec = mk_fs(plname,level)
	minetest.show_formspec(plname,"level_editor:main_gui_"..level, formspec)
	print("formspec showed: ".."level_editor:main_gui_"..level)
	return
end

minetest.register_chatcommand("level", {
	params = "level_name or nothing",
	privs = "level",
	description = "Éditer (ou créer) un niveau",
	func = function(sender,options)
		assert(type(options) == "string")
		level_editor.gui(sender,options)
	end
})

minetest.register_on_player_receive_fields(function(player,formname,fields)
	if formname ~= "level_editor.level_chooser" then
		return false
	end
	local plname = player:get_player_name()
	for k,v in pairs(fields) do
		if k ~= "quit" then
			level_editor.gui(plname,k)
			return
		end
	end
end)


minetest.register_chatcommand("levels", {
	params = "",
	privs = "level",
	description = "Choisis un niveau à modifier",
	func = function(sender)
		local formdef = level_editor.make_formspec("textures","level_")
		formdef = formdef..
			"label[2,0;Click on level to modify]"
		local formname = "level_editor.level_chooser"
		minetest.show_formspec(sender,formname,formdef)
	end
})
