local modpath = minetest.get_modpath(minetest.get_current_modname())

-- All about checkpoints



local on_punch_checkpoint = nil
on_punch_checkpoint = function(pos, node, clicker)
	local keys=clicker:get_player_control()
	local name = clicker:get_player_name()
	if minetest.check_player_privs(name, {level=true}) and keys["aux1"]==true then
		return -- if sneaking, on_punch calls normal destruct function
	else
		local strpos = minetest.pos_to_string(pos)
		level_editor.activate_checkpoint(name, pos, node)
		level_editor.go_checkpoint(name)
	end
end

local on_construct_checkpoint = nil
on_construct_checkpoint = function(pos)
	local meta = minetest.get_meta(pos)
	local formspec = level_editor.make_formspec("textures","info_")
	formspec = formspec..
		"label[2,0;Click on picture infos]"..
		"button_exit[13,0;2,1;ok;OK]"..
		"button_exit[13,1;2,1;clear;Clear]"
	meta:set_string("formspec",formspec)
--	meta:set_string("formspec",level_editor.message_formspec)
end

local on_receive_checkpoints_fields = nil
on_receive_checkpoints_fields = function(pos, formname, fields, placer)
	print("fields received : "..dump(fields))
	local name = placer:get_player_name()
	if not minetest.check_player_privs(name, {level=true}) then
		minetest.chat_send_player(name,"Tu n'as pas le droit de modifier le formulaire")
		return
	else
		local strpos = minetest.pos_to_string(pos)
		local chdata = level_editor.getcheckpointdata(strpos)
		local pldata = level_editor.getplayerdata(name)
		for k,v in pairs(fields) do
			if k ~= "quit" and k ~= "clear" and k~= "ok" then
				table.insert(chdata.infos,k)
				for i,v in ipairs(pldata.checkpoint) do
					if minetest.pos_to_string(v.pos) == minetest.pos_to_string(chdata.pos) then
						pldata.checkpoint[i] = table.copy(chdata)
						break
					end
				end
				print("break does not exit")
			elseif k == "clear" then
				chdata.infos = {}
			end
			level_editor.save()
			
			local settings = Settings(modpath.."/checkpoints/checkpoint_"..pos.x.."-"..pos.y..".conf")
			settings:set("pos",strpos)
			local infos = ""
			for i,v in ipairs(chdata.infos) do
				infos = infos..v..","
			end
			settings:set("infos",infos)
			local success = settings:write()

			minetest.chat_send_player(name,"Info "..#chdata.infos.." : "..k)
		end
	end
end

--level_editor.register_images_as_nodes("textures","checkpoint","info_","level")
level_editor.register_image_as_node("checkpoint")

level_editor.register_checkpoints_from_files = function()
	local checkpoint_dir = modpath.."/checkpoints/"
	local checkpoint_list = minetest.get_dir_list(checkpoint_dir,false)
	for i,v in ipairs(checkpoint_list) do
		if v:match("(%.conf)$") then
			local settings = Settings(checkpoint_dir..v)
			local conf = settings:to_table()
			local chdata = level_editor.getcheckpointdata(conf.pos)
			if conf.infos then
				local infos = {}
				for info in conf.infos:gmatch("[^,]+") do
					table.insert(infos,info)
				end
				chdata.infos = infos
			end
			print("dump chdata for "..conf.pos.."  :: "..dump(chdata))
			level_editor.save()
		end
	end
end
level_editor.register_checkpoints_from_files()

local custom_checkpoint_node = nil
custom_checkpoint_node = function()
	for nodename,nodedef in pairs(minetest.registered_nodes) do
		if nodename:match("level_editor:checkpoint") then
			minetest.override_item(nodename,{on_punch = on_punch_checkpoint})
			minetest.override_item(nodename,{on_construct = on_construct_checkpoint})
			minetest.override_item(nodename,{on_receive_fields = on_receive_checkpoints_fields})
			minetest.override_item(nodename,{groups = {oddly_breakable_by_hand=3, picture=1, not_in_craft_guide=1, not_in_creative_inventory=0}})
			break
		end
	end
end
custom_checkpoint_node()

	-- If, by accident, a player gets a checkpoint item, he can't place it
minetest.register_on_placenode(function(pos, newnode, placer, oldnode, itemstack, pointed_thing)
	if string.match(newnode.name,"^level_editor:checkpoint") then
		local plname = placer:get_player_name()
		if not minetest.check_player_privs(plname,{level=true}) then
			minetest.log("warning","player "..plname.." tried to place checkpoint node at "..minetest.pos_to_string(pos))
			minetest.set_node(pos,oldnode)--"default:air")
		else return
		end
	end
end)

function level_editor.activate_checkpoint(name,pos,node)
	local player = minetest.get_player_by_name(name)
	local pldata = level_editor.getplayerdata(name)
	local strpos = minetest.pos_to_string(pos)
	local chdata = level_editor.getcheckpointdata(strpos)
	table.insert(pldata.checkpoint, table.copy(chdata))
	print("checkpoint activated : "..dump(pldata.checkpoint[#pldata.checkpoint]))
	level_editor.save()
end

function level_editor.go_checkpoint(name)
	local player = minetest.get_player_by_name(name)
	local pldata = level_editor.getplayerdata(name)
	local checkpoint = pldata.checkpoint[#pldata.checkpoint]
	player:setpos(checkpoint.pos)
	minetest.log("action",name.." respawned at "..minetest.pos_to_string(checkpoint.pos).." with info : "..dump(checkpoint.infos))
	level_editor.show_next(name,checkpoint.infos)
end
