	-- Set unique global
level_editor = {}
level_editor.registered_levels = {}
level_editor.default_size = {x=50,y=50,z=50}


local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)
print("we are in : "..modpath)

	-- First thing, call load/save functions
dofile(modpath.."/gnapi/save.lua")
level_editor.load()

minetest.register_on_shutdown(function()
	level_editor.save()
end)

	-- Call utils
dofile(modpath.."/gnapi/utils.lua")
dofile(modpath.."/gnapi/selector.lua")
dofile(modpath.."/gnapi/shownext.lua")

	-- Settings in minetest.conf
minetest.settings:set_bool("enable_pvp",false)  -- can be changed durig game
minetest.settings:set_bool("enable_damage",true) -- can be changed durig game
-- minetest.settings:set("fixed_map_seed","legaragenumerique")  -- not useful because minetest.set_mapgen_setting handles it in mapping.lua
--minetest.settings:set("mg_name","v5")  -- is overriden by GUI
--minetest.settings:set_bool("creative_mode",false)
minetest.settings:set_bool("projecting_dungeons",false)  -- don't know if it works
minetest.settings:write()


	-- Register privileges
minetest.register_privilege("level", {description = "Able to place levels and checkpoints", give_to_singleplayer= false})

	-- Calls checkpoints.lua
dofile(minetest.get_modpath("level_editor").."/src/checkpoints.lua")

	-- Calls mapping.lua
dofile(minetest.get_modpath("level_editor").."/src/protection.lua")

	-- Calls mapping.lua
dofile(minetest.get_modpath("level_editor").."/src/mapping.lua")

	-- Calls editor.lua
dofile(minetest.get_modpath("level_editor").."/src/editor.lua")

	-- Calls levels.lua
dofile(minetest.get_modpath("level_editor").."/src/levels.lua")

minetest.register_on_respawnplayer(function(player)
	local name = player:get_player_name()
	local pldata = level_editor.getplayerdata(name)
	if pldata.actual_level == "" then
		minetest.log("action","no previous level accessed by "..name.." on respawn")
		return
	else
		minetest.log("action","level accessed by "..name.." on respawn : "..pldata.actual_level)
		local pos = pldata.checkpoint[#pldata.checkpoint]
		local hp = pldata.registered_levels[pldata.actual_level].hp
		level_editor.go_checkpoint(name)
		if hp then player:set_hp(hp) end
		return true
	end
end)

minetest.register_on_newplayer(function(player)
	local name = player:get_player_name()
	local pldata = level_editor.getplayerdata(name)
	if pldata.actual_level == "" then
		level_editor.go_level(name,"home")
		return
	end
end)

	-- End of init
