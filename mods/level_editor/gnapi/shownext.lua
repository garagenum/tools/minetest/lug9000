level_editor.checkpoint_hud_index = {}

if minetest.get_modpath("controls") ~= nil then 
	controls.register_on_press(function(player,key)
		local name = player:get_player_name()
		local pldata = level_editor.getplayerdata(name)
		local infos = pldata.checkpoint[#pldata.checkpoint].infos
		if key == "jump" then
			level_editor.checkpoint_hud_index[name] = level_editor.checkpoint_hud_index[name] or {}
			local i = level_editor.checkpoint_hud_index[name].count or 1
			print("i :: "..i)
			local hud_id = level_editor.checkpoint_hud_index[name].id or 1
			if not infos then infos = {} end
			if i < #infos then
				i = i+1
				level_editor.checkpoint_hud_index[name].count = i
				minetest.after(0.1, function()
					player:hud_change(hud_id,"text","info_"..infos[i]..".png")
				end)
			else
				player:hud_remove(hud_id)
				level_editor.checkpoint_hud_index[name].count = 1
			end
		end
	end)
end

level_editor.show_next = function(name,infos)
	local player = minetest.get_player_by_name(name)

	local hud_ind = player:hud_add({
		hud_elem_type = "image",
		position = {x=0.5,y=0.5},
		offset = {x=0,y=0},
		text = "",
		scale = {x=1,y=1},
		alignement = {x=0,y=0},
		})
	level_editor.checkpoint_hud_index[name] = level_editor.checkpoint_hud_index[name] or {}
	level_editor.checkpoint_hud_index[name].id = hud_ind
	
	local ja_shown = 1
	if #infos == 0 then player:hud_remove(hud_ind) return end
	print("infos :: "..dump(infos).." :: "..#infos)
	print("hud_ind "..hud_ind)
	print("jashown :: "..ja_shown.." - "..infos[ja_shown])
--	player:hud_change(hud_ind,"text","info_"..infos[ja_shown]..".png")
	print("with def :: "..dump(player:hud_get(hud_ind)))
	if minetest.get_modpath("controls") ~= nil then
		local i = 1
		minetest.after(0.1, function()
			player:hud_change(hud_ind,"text","info_"..infos[i]..".png")
			level_editor.checkpoint_hud_index[name].count = 1
		end)
	else
		for i=1, #infos do
			minetest.after(2*(i-0.9), function()
				player:hud_change(hud_ind,"text","info_"..infos[i]..".png")
			end)
		end
		minetest.after(#infos*2, function()
			player:hud_remove(hud_ind)
		end)
	end
end
