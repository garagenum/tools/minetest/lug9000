## Comment installer et configurer le jeu Minetest du Garage Numérique

Il faut d'abord installer Minetest.
**Pour le moment le jeu du Garage Numérique n'est pas compatible avec les versions antérieures à Minetest 5 !
~~Si vous jouez seul, vous pouvez installer la dernière version stable de Minetest.~~
~~Si vous jouez en réseau, il faudra installer Minetest 5 si le serveur utilise Minetest 5~~

* [Installer Minetest 5 (dernière version stable) sous Windows](install-minetest-5-windows.md)  
* [Installer Minetest 5 (dernière version stable) sous Linux](install-minetest-5-linux.md)  
* [Installer Minetest 4.17 (version stable précédente) sous Linux]  
* [Installer Minetest 5 (version en développement) sous Mac OSX] (install-minetest-5-macosx.md) 
* [Installer Minetest 4.17 (dernière version stable) sous Mac OSX]  

Une fois Minetest installé, vous devez télécharger le jeu Minetest du Garage Numérique sous forme d'[archive zip](https://gitlab.com/garagenum/minetest-garage/-/archive/master/minetest-garage-master.zip) et l'extraire dans le dossier "games" de Minetest (cf pages Installation).

Pour utiliser le jeu, il y a 3 possibilités:
*
