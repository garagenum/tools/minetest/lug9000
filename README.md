Le projet Minetest du Garage numérique
==========

Le Garage numérique est une association basée dans le 20e arrondissement à Paris, qui travaille à la promotion des outils numériques libres dans son quartier.

Ce projet vise à développer notre propre subgame Minetest en vue d'un usage éducatif, à destination notamment des écoles environnantes.

Les deux mods que nous développons pour cela sont mcq_editor , qui permet de mettre en place un QCM dans le jeu,  
et level_editor , qui permet d'éditer des niveaux.

Utilisez la commande /level ou cherchez les items mcq, level ou checkpoint

Liens
==========

http://www.legaragenumerique.fr/


A Minetest project by Le Garage numérique
==========

Le Garage numérique is an association located in the 20th district of Paris (France), that seeks to promote opensource digital tools in its area.

This project aims at developping our own Minetest subgame for educational purposes, to be especially used in the surrounding schools.

The two mods we are developping are:  
* mcq_editor
* level_editor

Use command /level in-game , or look for nodes mcq, checkpoint and level.

Links
===============

http://www.legaragenumerique.fr/

Documentation
===========

See [Wiki](https://gitlab.com/garagenum/minetest-garage/wikis/home)


Work in Progress
==========

See dev branch
